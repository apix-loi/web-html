## Modified version of Plyr.
This is a modified version of Plyr with a yellow theme.

[Checkout the demo](https://speedy.eu.org/player/). This is specially for ```HTML5 Video Player```.

# Installation
## HTML5 Video
```html
<div class="plyr-responsive" id="container">
  <video height="400"
         controls data-poster="img/poster.jpg"
         class="responsive plyr-player">
    
    <!-- Video files -->
    <source src="sample.mp4" type="video/mp4" size="480">
    <source src="sample.mp4" type="video/mp4" size="720">
    <source src="sample.mp4" type="video/mp4" size="1080">
    
    <!-- Caption files -->
    <track kind="captions" label="English" srclang="en" src="sample.vtt" default />
  </video>
</div>
```
Replace `sample.mp4` with your video url or name.
Replace `sample.vtt` with your captions url or name.

**Note**: The poster image should be specified using `data-poster`. This is to prevent it [being downloaded twice](https://github.com/sampotts/plyr/issues/1531). If you're sure the image will be cached, you can still use the `poster` attribute for true progressive enhancement.
## CSS
You can install the stylesheet using this code in ```<head>```
```html
<link rel="stylesheet" href="https://apicloud.eu.org/plyr/style.css" />
```
...or use this if above doesn't work :
```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/apix-loi/jsdelivr/plyr/style.css" />
```
## JavaScript
You can install the javascript using this code before closing ```</body>```
```html
<script src="https://apicloud.eu.org/plyr/script.js"></script>
```
...or use this if above doesn't work :
```html
<script src="https://cdn.jsdelivr.net/gh/apix-loi/jsdelivr/plyr/script.js"></script>
```
## Buttons
At last you will have to add this code just below the JavaScript.
```html
<script>
  var controls =
[
    'play-large', // The large play button in the center
   // 'restart', // Restart playback
    'rewind', // Rewind by the seek time (default 10 seconds)
    'play', // Play/pause playback
    'fast-forward', // Fast forward by the seek time (default 10 seconds)
    'progress', // The progress bar and scrubber for playback and buffering
    'current-time', // The current time of playback
    'duration', // The full duration of the media
   //'volume', // Volume control
   // 'captions', // Toggle captions
    'mute', // Toggle mute
    'settings', // Settings menu
   // 'pip', // Picture-in-picture (currently Safari only)
    'airplay', // Airplay (currently Safari only)
    'download', // Show a download button with a link to either the current source or a custom URL you specify in your options
    'fullscreen' // Toggle fullscreen
];

  const player = new Plyr('.plyr-player',{controls});
</script>
```
To add or remove buttons you will have to just add or remove `//` from the code above.

*View code on **[Tryit Editor]** online*(https://www.w3schools.com/code/tryit.asp?filename=GTHHSAE4MPJZ)
